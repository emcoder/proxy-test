# Proxy Test
Simple reverse proxy for single web resource written in plain Node/Typescript using standard sockets.  
Test task for Node.ts vacancy.

## Setup
Application is configurable using environment variables:

* `PROXY_HOST` — application host *(default: `localhost`)*
* `PROXY_PORT` — application port *(default: `80`)*

Target remote resource configuration:

* `TARGET_HOST` — target host *(default: `browserleaks.com`)*
* `TARGET_PORT` — target port *(default for HTTPS: `443`)*

## Run
To start application, run inside root directory *(superuser rights may be required)*:
```
yarn start
```
Navigate to [localhost](http://localhost/) in your browser to see proxied remote host.  

For development use:
```
yarn dev
```

## Deploy
Docker can be used for deployment.  
Run inside app root to build docker image using `Dockerfile`:
```
docker build -t username/image .
```
And start container:
```bash
docker run -it --rm -p 80:80 username/image
```
import { Socket } from "net";
import { TLSSocket } from "tls";

import { TARGET_HOST, TARGET_PORT, TLS } from "./conf";
import log from "./log";
import { setHostHeader } from "./utils";

// Proxy client request to target host
function process(client: Socket) {
  log.info("Client request accepted");

  const raw = new Socket();
  let target = raw;

  if (TLS) {
    log.info("Using TLS socket for target");
    target = new TLSSocket(raw);
  }

  raw.connect(TARGET_PORT, TARGET_HOST, () => {
    // On client data recived, pass it to target host replacing `Host` header
    client.on("data", (data) => {
      log.data("client", "proxy", data);
      const resData = setHostHeader(data, TARGET_HOST);

      log.data("proxy", "target", resData);
      writeSock(target, resData);
    });

  });

  // Remote target response data, pass to client
  target.on("data", (data) => {
    log.data("target", "proxy", data);
    log.data("proxy", "client", data);
    writeSock(client, data);
  });

  client.on("close", (err) => {
    log.info("Client socket closed");
    if (err) {
      log.warn("Client socket was closed due to error");
    }
    target.end();
    raw.end();
  });

}

function writeSock(toSock: Socket, data: Buffer) {
  if (toSock.destroyed) {
    log.warn("Can't write data to destroyed socket");
    return;
  }

  try {
    log.data("proxy", "target", data);
    toSock.write(data);
  } catch (err) {
    log.error("Failed write to socket:", err.message);
  }
}

export default process;

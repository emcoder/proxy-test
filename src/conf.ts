const PROXY_HOST = process.env.PROXY_HOST || "localhost";
const PROXY_PORT = parseInt(process.env.PROXY_PORT, 10) || 80;

const TARGET_HOST = process.env.TARGET_HOST || "browserleaks.com";
const TARGET_PORT = parseInt(process.env.TARGET_PORT, 10) || 443;

// TODO: Auto-detect? Use .env file?
const TLS = TARGET_PORT === 443;

export {
  PROXY_HOST,
  PROXY_PORT,
  TARGET_HOST,
  TARGET_PORT,
  TLS,
};

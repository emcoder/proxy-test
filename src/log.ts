// Simple logging functions

function currentDate(): string {
  const now = new Date();
  const date = now.toISOString().replace(/T/, " ").replace(/\..+$/, "");
  return date;
}

function error(...args: any[]) {
  const date = currentDate();
  console.warn(`[${date}] [ERROR]`, ...args);
}

function warn(...args: any[]) {
  const date = currentDate();
  console.warn(`[${date}] [WARNING]`, ...args);
}

function info(...args: any[]) {
  const date = currentDate();
  console.info(`[${date}] [INFO]`, ...args);
}

// Log data buffer with text about it source and destination
function data(from: string, to: string, buffer: Buffer) {
  const size = buffer.length;
  info(`(${from.toUpperCase()} ⇒ ${to.toUpperCase()}): ${size} bytes`);
}

export default {
  info,
  data,
  log: info,
  warn,
  error,
};
